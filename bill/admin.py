from django.contrib import admin

from .models import Bill, Entry


class EntryInline(admin.TabularInline):
    model = Entry
    extra = 0


class BillAdmin(admin.ModelAdmin):
    inlines = [EntryInline]


admin.site.register(Bill, BillAdmin)
admin.site.register(Entry)
