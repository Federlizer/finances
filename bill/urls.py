from django.urls import path

from . import views


app_name = "bill"

urlpatterns = [
    path("", views.BillList.as_view(), name="list-create"),
    path("<uuid:pk>/", views.BillDetail.as_view(), name="detail-update-delete"),
]
