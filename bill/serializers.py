from rest_framework import serializers

from . import models


class EntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Entry
        fields = ["id", "category", "amount"]


class BillSerializer(serializers.ModelSerializer):
    entries = EntrySerializer(many=True)

    class Meta:
        model = models.Bill
        fields = ["id", "date", "notes", "currency", "location", "entries"]

    def validate_entries(self, entries):
        if not entries:
            raise serializers.ValidationError("Must have at least one entry")
        categories_included = set([entry["category"].pk for entry in entries])
        if len(categories_included) != len(entries):
            # We have an entry with a duplicate category
            raise serializers.ValidationError("Each entry must have a unique category")
        return entries

    def create(self, validated_data):
        entries_data = validated_data.pop("entries")
        bill = models.Bill.objects.create(**validated_data)
        for entry_data in entries_data:
            models.Entry.objects.create(bill=bill, **entry_data)
        return bill

    def update(self, bill, validated_data):
        bill.date = validated_data["date"]
        bill.notes = validated_data["notes"]
        bill.currency = validated_data["currency"]
        bill.location = validated_data["location"]
        # Handle entries
        bill.entries.all().delete()
        for entry in validated_data["entries"]:
            models.Entry.objects.create(bill=bill, **entry)
        bill.save()
        return bill
