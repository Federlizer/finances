from rest_framework import generics

from . import models, serializers


class BillList(generics.ListCreateAPIView):
    queryset = models.Bill.objects.all()
    serializer_class = serializers.BillSerializer


class BillDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Bill.objects.all()
    serializer_class = serializers.BillSerializer
    http_method_names = ["get", "put", "delete"]
