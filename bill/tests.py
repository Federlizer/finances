from decimal import Decimal

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from category.models import Category
from currency.models import Currency
from location.models import Location
from .models import Bill, Entry


class BillModelTests(APITestCase):

    def setUp(self):
        self.currency = Currency.objects.create(name="test_currency", code="TC", symbol="tc.")
        self.location = Location.objects.create(name="test_location")

    def test_total_amount_no_entries(self):
        bill = Bill.objects.create(currency=self.currency, location=self.location)
        self.assertEqual(bill.total_amount(), 0)

    def test_total_amount_one_entry(self):
        bill = Bill.objects.create(currency=self.currency, location=self.location)
        cat_one = Category.objects.create(name="test_category_one")
        Entry.objects.create(bill=bill, category=cat_one, amount=12.45)
        self.assertEqual(bill.total_amount(), Decimal('12.45'))

    def test_total_amount_two_entries(self):
        bill = Bill.objects.create(currency=self.currency, location=self.location)
        cat_one = Category.objects.create(name="test_category_one")
        cat_two = Category.objects.create(name="test_category_two")
        Entry.objects.create(bill=bill, category=cat_one, amount=12.45)
        Entry.objects.create(bill=bill, category=cat_two, amount=25.99)
        self.assertEqual(bill.total_amount(), Decimal('38.44'))


class BillCreateViewTests(APITestCase):

    def setUp(self):
        self.currency = Currency.objects.create(name="test_currency", code="TC", symbol="tc.")
        self.location = Location.objects.create(name="test_location")
        self.category_one = Category.objects.create(name="test_category_one")
        self.category_two = Category.objects.create(name="test_category_two")

    def test_does_not_create_bill_no_entries(self):
        url = reverse("bill:list-create")
        data = {
            "date": "2024-06-09",
            "currency": self.currency.id,
            "location": self.location.id,
            "entries": [],
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Bill.objects.count(), 0)
        self.assertEqual(Entry.objects.count(), 0)

    def test_creates_bill_one_entry(self):
        url = reverse("bill:list-create")
        data = {
            "date": "2024-06-09",
            "currency": self.currency.id,
            "location": self.location.id,
            "entries": [
                {"category": self.category_one.id, "amount": 11.11},
            ],
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Bill.objects.count(), 1)
        self.assertEqual(Entry.objects.count(), 1)

    def test_creates_bill_two_entries(self):
        url = reverse("bill:list-create")
        data = {
            "date": "2024-06-09",
            "currency": self.currency.id,
            "location": self.location.id,
            "entries": [
                {"category": self.category_one.id, "amount": 11.11},
                {"category": self.category_two.id, "amount": 22.22},
            ],
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Bill.objects.count(), 1)
        self.assertEqual(Entry.objects.count(), 2)

    def test_does_not_create_bill_two_entries_same_category(self):
        url = reverse("bill:list-create")
        data = {
            "date": "2024-06-09",
            "currency": self.currency.id,
            "location": self.location.id,
            "entries": [
                {"category": self.category_one.id, "amount": 11.11},
                {"category": self.category_one.id, "amount": 22.22},
            ],
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Bill.objects.count(), 0)
        self.assertEqual(Entry.objects.count(), 0)


class BillUpdateViewTests(APITestCase):

    def setUp(self):
        self.currency = Currency.objects.create(name="test_currency", code="TC", symbol="tc.")
        self.location = Location.objects.create(name="test_location")
        self.category_one = Category.objects.create(name="test_category_one")
        self.category_two = Category.objects.create(name="test_category_two")
        self.category_three = Category.objects.create(name="test_category_three")
        self.bill = Bill.objects.create(date="2024-06-09", currency=self.currency, location=self.location)
        self.entry_one = Entry.objects.create(bill=self.bill, category=self.category_one, amount=11.11)
        self.entry_two = Entry.objects.create(bill=self.bill, category=self.category_two, amount=22.22)

    def test_updates_adding_one_entry(self):
        url = reverse("bill:detail-update-delete", kwargs={"pk": self.bill.id})
        updated_entries = [
            {"category": self.entry_one.category.id, "amount": self.entry_one.amount},
            {"category": self.entry_two.category.id, "amount": self.entry_two.amount},
            # New entry
            {"category": self.category_three.id, "amount": 33.33},
        ]
        data = {
            "date": "2023-07-08",
            "notes": "updated for testing",
            "currency": self.bill.currency.id,
            "location": self.bill.location.id,
            "entries": updated_entries,
        }
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_bill = Bill.objects.get(pk=self.bill.id)
        self.assertEqual(updated_bill.currency.id, data["currency"])
        self.assertEqual(str(updated_bill.date), data["date"])
        self.assertEqual(updated_bill.location.id, data["location"])
        self.assertEqual(updated_bill.notes, data["notes"])
        self.assertEqual(updated_bill.entries.count(), len(updated_entries))
        updated_bill.entries.get(amount=self.entry_one.amount)
        updated_bill.entries.get(amount=self.entry_two.amount)
        updated_bill.entries.get(amount=33.33)
        self.assertEqual(Entry.objects.count(), 3)

    def test_updates_removing_one_entry(self):
        url = reverse("bill:detail-update-delete", kwargs={"pk": self.bill.id})
        updated_entries = [
            {"category": self.entry_one.category.id, "amount": self.entry_one.amount},
        ]
        data = {
            "date": "2023-07-08",
            "notes": "updated for testing",
            "currency": self.bill.currency.id,
            "location": self.bill.location.id,
            "entries": updated_entries,
        }
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_bill = Bill.objects.get(pk=self.bill.id)
        self.assertEqual(updated_bill.currency.id, data["currency"])
        self.assertEqual(str(updated_bill.date), data["date"])
        self.assertEqual(updated_bill.location.id, data["location"])
        self.assertEqual(updated_bill.notes, data["notes"])
        self.assertEqual(updated_bill.entries.count(), len(updated_entries))
        updated_bill.entries.get(amount=self.entry_one.amount)
        with self.assertRaises(Entry.DoesNotExist):
            updated_bill.entries.get(amount=self.entry_two.amount)
        self.assertEqual(Entry.objects.count(), 1)

    def test_updates_removing_one_entry_adding_one_entry(self):
        url = reverse("bill:detail-update-delete", kwargs={"pk": self.bill.id})
        updated_entries = [
            {"category": self.entry_one.category.id, "amount": self.entry_one.amount},
            # New entry
            {"category": self.category_three.id, "amount": 33.33},
        ]
        data = {
            "date": "2023-07-08",
            "notes": "updated for testing",
            "currency": self.bill.currency.id,
            "location": self.bill.location.id,
            "entries": updated_entries,
        }
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_bill = Bill.objects.get(pk=self.bill.id)
        self.assertEqual(updated_bill.currency.id, data["currency"])
        self.assertEqual(str(updated_bill.date), data["date"])
        self.assertEqual(updated_bill.location.id, data["location"])
        self.assertEqual(updated_bill.notes, data["notes"])
        self.assertEqual(updated_bill.entries.count(), len(updated_entries))
        updated_bill.entries.get(amount=self.entry_one.amount)
        updated_bill.entries.get(amount=33.33)
        with self.assertRaises(Entry.DoesNotExist):
            updated_bill.entries.get(amount=self.entry_two.amount)
        self.assertEqual(Entry.objects.count(), 2)

    def test_updates_changing_one_entry(self):
        url = reverse("bill:detail-update-delete", kwargs={"pk": self.bill.id})
        updated_entries = [
            {"category": self.entry_one.category.id, "amount": self.entry_one.amount},
            # Changed entry
            {"category": self.entry_two.category.id, "amount": 44.44},
        ]
        data = {
            "date": "2023-07-08",
            "notes": "updated for testing",
            "currency": self.bill.currency.id,
            "location": self.bill.location.id,
            "entries": updated_entries,
        }
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_bill = Bill.objects.get(pk=self.bill.id)
        self.assertEqual(updated_bill.currency.id, data["currency"])
        self.assertEqual(str(updated_bill.date), data["date"])
        self.assertEqual(updated_bill.location.id, data["location"])
        self.assertEqual(updated_bill.notes, data["notes"])
        self.assertEqual(updated_bill.entries.count(), len(updated_entries))
        updated_bill.entries.get(amount=self.entry_one.amount)
        updated_bill.entries.get(amount=44.44)
        with self.assertRaises(Entry.DoesNotExist):
            updated_bill.entries.get(amount=self.entry_two.amount)
        self.assertEqual(Entry.objects.count(), 2)

    def test_does_not_update_no_entries(self):
        url = reverse("bill:detail-update-delete", kwargs={"pk": self.bill.id})
        updated_entries = []
        data = {
            "date": "2023-07-08",
            "notes": "updated for testing",
            "currency": self.bill.currency.id,
            "location": self.bill.location.id,
            "entries": updated_entries,
        }
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        updated_bill = Bill.objects.get(pk=self.bill.id)
        self.assertNotEqual(str(updated_bill.date), data["date"])
        self.assertNotEqual(updated_bill.notes, data["notes"])
        # 2 is the original count of entries from setUp
        self.assertEqual(updated_bill.entries.count(), 2)
        updated_bill.entries.get(amount=self.entry_one.amount)
        updated_bill.entries.get(amount=self.entry_two.amount)
        self.assertEqual(Entry.objects.count(), 2)

    def test_does_not_update_bill_two_entries_same_category(self):
        url = reverse("bill:detail-update-delete", kwargs={"pk": self.bill.id})
        updated_entries = [
            {"category": self.entry_one.category.id, "amount": self.entry_one.amount},
            {"category": self.entry_two.category.id, "amount": self.entry_two.amount},
            # New entry, duplicate category
            {"category": self.category_one.id, "amount": 33.33},
        ]
        data = {
            "date": "2023-07-08",
            "notes": "updated for testing",
            "currency": self.bill.currency.id,
            "location": self.bill.location.id,
            "entries": updated_entries,
        }
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        updated_bill = Bill.objects.get(pk=self.bill.id)
        self.assertNotEqual(str(updated_bill.date), data["date"])
        self.assertNotEqual(updated_bill.notes, data["notes"])
        # w is the original count of entries from setUp
        self.assertEqual(updated_bill.entries.count(), 2)
        updated_bill.entries.get(amount=self.entry_one.amount)
        updated_bill.entries.get(amount=self.entry_two.amount)
        with self.assertRaises(Entry.DoesNotExist):
            updated_bill.entries.get(amount=33.33)
        self.assertEqual(Entry.objects.count(), 2)
