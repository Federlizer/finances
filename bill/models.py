import uuid

from django.utils import timezone
from django.db import models

from currency.models import Currency
from category.models import Category
from location.models import Location


class Bill(models.Model):

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    date = models.DateField(default=timezone.now)
    notes = models.CharField(max_length=500, blank=True)
    currency = models.ForeignKey(Currency, on_delete=models.PROTECT)
    location = models.ForeignKey(Location, on_delete=models.PROTECT)

    def __str__(self):
        amount_n_currency = f"{self.total_amount()} {self.currency.symbol}"
        return f"{self.date}, {self.location.name} ({amount_n_currency})"

    def total_amount(self):
        """Calculate the total amount of money on this bill."""

        return sum([entry.amount for entry in self.entries.all()])


class Entry(models.Model):

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    bill = models.ForeignKey(Bill, related_name="entries", on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=20, decimal_places=2)

    class Meta:
        verbose_name_plural = "entries"
        constraints = [
            models.UniqueConstraint(fields=["bill", "category"], name="unique_category_per_bill"),
        ]
