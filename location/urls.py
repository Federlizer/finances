from django.urls import path

from . import views


app_name = "location"

urlpatterns = [
    path("", views.LocationList.as_view(), name="list"),
    path("<uuid:pk>/", views.LocationDetail.as_view(), name="detail"),
]
