import uuid

from django.db import models


class Location(models.Model):
    """A location where a bill took place."""

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.name} ({self.id})"
