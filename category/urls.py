from django.urls import path

from . import views

app_name = "category"

urlpatterns = [
    path("", views.CategoryList.as_view(), name="list"),
    path("<uuid:pk>/", views.CategoryDetail.as_view(), name="detail"),
]
