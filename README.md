

## Known issues:

- [ ] Straight up deleting a currency that is used by a Bill returns a 500 Internal Server Error.
      This is likely the case for Category and Location as well.
