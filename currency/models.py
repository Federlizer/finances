import uuid

from django.db import models


class Currency(models.Model):
    """A real-life currency."""

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=50, unique=True)
    # ISO-4217 code - https://www.iso.org/iso-4217-currency-codes.html
    code = models.CharField(max_length=50, unique=True)
    symbol = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "currencies"

    def __str__(self):
        return f"{self.name} ({self.id})"
