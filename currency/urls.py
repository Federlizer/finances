from django.urls import path

from . import views


app_name = "currency"

urlpatterns = [
    path("", views.CurrencyList.as_view(), name="list"),
    path("<uuid:pk>/", views.CurrencyDetail.as_view(), name="detail"),
]
